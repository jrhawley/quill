{ pkgs, craneLib, fenix, ... }: let
  dev-deps = (with pkgs; [
    alejandra
    cachix
    cargo-audit
    cargo-deny
    cargo-edit
    cargo-make
    cargo-machete
    cargo-msrv
    cargo-nextest
    cargo-watch
    glab
    gitui
    helix
    jq
    p7zip
    nil
    rust-analyzer
    treefmt
  ]);

  rust-deps = (with pkgs; [
    fenix
    openssl
    pkg-config
  ]);
in craneLib.devShell {
  name = "quill";
  packages = dev-deps ++ rust-deps;
}
