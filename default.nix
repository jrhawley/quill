{ pkgs, craneLib, ... }: let
  inherit (pkgs) lib stdenv;

  pname = "quill";
  src = ./.;

  commonArgs = {
    inherit pname src;
    strictDeps = true;

    buildInputs = [
    ] ++ lib.optionals stdenv.isDarwin [
      pkgs.libiconv
    ];
  };

  cargoArtifacts = craneLib.buildDepsOnly commonArgs;
in craneLib.buildPackage (
  commonArgs // {
    inherit cargoArtifacts;
    cargoExtraArgs = "--bin quill";
  }
)

