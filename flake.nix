{
  description = "Query all your bills and accounts to check on your financial statements.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-parts.url = "github:hercules-ci/flake-parts";
    advisory-db = {
      url = "github:rustsec/advisory-db";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, crane, fenix, flake-parts, advisory-db, ... }@inputs: flake-parts.lib.mkFlake { inherit inputs; } {
    flake = { };

    systems = [
      "x86_64-linux"
      "x86_64-darwin"
    ];

    perSystem = { config, pkgs, system, ...}: {
      devShells.default = import ./shell.nix { 
        inherit pkgs;
        craneLib = (crane.mkLib pkgs);
        fenix = fenix.packages.${system}.stable.withComponents [
          "cargo"
          "clippy"
          "llvm-tools"
          "rust-src"
          "rustc"
        ];
      };

      formatter = pkgs.alejandra;

      packages.default = import ./default.nix {
        inherit pkgs;
        craneLib = (crane.mkLib pkgs);
      };
    };
  };
}
